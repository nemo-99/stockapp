
import yfinance as yf
from patterns import Allpatterns,volume,send_mail

portfolio = ["TCS.NS","HEROMOTOCO.NS","BRITANNIA.NS","ULTRACEMCO.NS",
             "BAJAJ-AUTO.NS","TATASTEEL.NS","ITC.NS","NTPC.NS","TECHM.NS",
             "CIPLA.NS","SHREECEM.NS","LT.NS","GRASIM.NS","ONGC.NS","MARUTI.NS",
             "COALINDIA.NS","NESTLEIND.NS","RELIANCE.NS","ZEEL.NS","BAJAJFINSV.NS",
             "KOTAKBANK.NS","BAJFINANCE.NS","HINDALCO.NS","TITAN.NS","INDUSINDBK.NS",
             "ICICIBANK.NS","M&M.NS","BHARTIARTL.NS","GAIL.NS","WIPRO.NS","BPCL.NS",
            "ASIANPAINT.NS","ADANIPORTS.NS","HCLTECH.NS","HDFC.NS","HDFCBANK.NS",
            "HINDUNILVR.NS","IOC.NS","INFY.NS","JSWSTEEL.NS","POWERGRID.NS","SBIN.NS",
             "SUNPHARMA.NS","UPL.NS","VEDL.NS","AXISBANK.NS","TATAMOTORS.NS",
             "INFRATEL.NS","DRREDDY.NS"
            ]
portfolio.sort()
data = yf.download(' '.join(portfolio),period="10d",threads =
                   True,group_by="ticker")
total = 0
with open("analysis.txt","w+") as f:
    for stocks in portfolio:
        vol = volume(data[stocks].Volume.values)
        if vol < 0:
            continue
        pattern = Allpatterns(data[stocks].iloc[-1:-4:-1])
        res = pattern.check_patterns()
        if res == False:
            continue
        f.write("*************************\n")
        f.write("Stock Name = %s\n"%stocks)
        f.write("Volume above average = %f"%vol)
        f.write(str(pattern))
        f.write("*************************\n")
        total +=1

    f.write("Total stocks shortlisted = %s\n"%total)


#send_mail()


