# coding: utf-8

import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders


def send_mail(send_from="Stock Analyzer" ,
              send_to=["",""],
              subject="Stock Report", message="PFA your stock report",
              files=["./analysis.txt"],server="smtp.gmail.com", port=587, 
              username="", password='',
              use_tls=True):
    """Compose and send email with provided info and attachments.

    Args:
        send_from (str): from name
        send_to (list[str]): to name(s)
        subject (str): message title
        message (str): message body
        files (list[str]): list of file paths to be attached to email
        server (str): mail server host name
        port (int): port number
        username (str): server auth username
        password (str): server auth password
        use_tls (bool): use TLS mode
    """
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(message))

    for path in files:
        part = MIMEBase('application', "octet-stream")
        with open(path, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(Path(path).name))
        msg.attach(part)

    smtp = smtplib.SMTP(server, port)
    if use_tls:
        smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()





def volume(data):
    return (((data[-1] - data.sum()/data.shape[0] ) / data[-1] )*100)




class SinglePatterns:
    def __init__(self,data):
        self.body = data.Close - data.Open
        self.ushadow = data.High - data.Open if self.body < 0 else data.High - data.Close
        self.lshadow = data.Close - data.Low if self.body < 0 else data.Open - data.Low
        self.high = data.High
        self.low = data.Low
        self.op = data.Open
        self.close = data.Close
        self.ss = False
        self.umb = False
        self.st = False
        self.dj = False

    def umbrella(self):
        self.umb = self.lshadow/abs(self.body) > 2
        return self.umb

    def shooting_star(self):
        self.ss =  self.ushadow/abs(self.body) > 2
        return self.ss
    
    def spinning_top(self):
        top_boundary = 0.30
        self.st = (self.ushadow > abs(self.body)*top_boundary) & \
                (self.lshadow >  abs(self.body)*top_boundary) & \
                 (min(self.ushadow,self.lshadow) > \
                top_boundary*max(self.ushadow,self.lshadow))
        return self.st

    
    def doji(self):
        self.dj = self.shooting_star() and self.spinning_top() and\
        self.umbrella()
        return self.dj

    def __str__(self):
        out = "\n"
        out +="Body = %f UShadow = %f LShadow = \
              %f \n"%(self.body,self.ushadow,self.lshadow)
        out +="Shooting star = %r\n"%self.ss
        out +="Umbrella = %r\n"%self.umb
        out +="Spinning Top = %r\n"%self.st
        out += "Doji = %r\n"%self.dj
        return out

    def set_patterns(self):
        self.umbrella()
        self.shooting_star()
        self.spinning_top()
        self.doji()
        if self.dj == True:
            self.umb = False
            self.ss = False
            self.st = False
            return
        self.umb = self.umb if not self.st else False
        self.ss = self.ss if not self.st else False

    def check_patterns(self):
        self.set_patterns()
        return self.ss or self.umb

class Allpatterns:
    def __init__(self,data):
        self.today = SinglePatterns(data.iloc[0])
        self.prev1 = SinglePatterns(data.iloc[1])
        self.prev2 = SinglePatterns(data.iloc[2])
        self.traj_dict = {"bullish":True,"bearish":True,"absent":False }
        self.eng = "absent"
        self.hrm = "absent"
        self.star = "absent"
        self.single_check = "absent"

    def engulfing_check(self,day1,day2):
         if day1.body*day2.body > 0:
            return "absent"
         if day2.body < 0:
            if day1.close <= day2.op and day1.op >=\
            day2.close:
                return "bearish"
         else:
            if day1.op <= day2.close and day1.close >= \
               day2.op:
                return "bullish"
         return "absent"

    def harami(self):
        out = self.engulfing_check(self.today,self.prev1)
        if out  == "bearish":
            return "bullish"
        elif out == "bullish":
            return "bearish"
        else:
            return out
            
    
    def engulfing(self):
        return self.engulfing_check(self.prev1,self.today)

    def star_check(self):
        if (self.prev2.body < 0) and (self.prev2.close > self.prev1.op) \
           and (self.prev1.doji() or self.prev1.spinning_top()) \
           and (self.today.op > max(self.prev1.close,self.prev1.op)) and \
           (self.today.body > 0):
            return "bullish"
        elif (self.prev2.body > 0) and (self.prev2.close < self.prev1.op)\
                and (self.prev1.doji() or self.prev1.spinning_top()) \
                and (self.today.op < min(self.prev1.close,self.prev1.op)) and \
                (self.today.body < 0):
            return "bearish"
        else:
            return "absent"


    def set_patterns(self):
        self.single_check = self.today.check_patterns()
        self.hrm =  self.harami()
        self.star = self.star_check()
        self.eng = self.engulfing()
        
    def check_patterns(self):
        self.set_patterns()
        if self.traj_dict[self.eng] or \
           self.traj_dict[self.hrm] \
           or self.traj_dict[self.star] \
           or self.single_check:
            return True
        else: 
            return False

    def __str__(self):
        out = str(self.today)
        out += "Engulfing = %s\n"%self.eng
        out += "Harami = %s\n"%self.hrm
        out += "Star = %s\n"%self.star
        return out

